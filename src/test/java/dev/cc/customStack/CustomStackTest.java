package dev.cc.customStack;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CustomStackTest {

    @Test
    public void testCustomStackPushObject(){
        CustomStack customStack = new CustomStack();
        Assertions.assertAll(
                ()-> Assertions.assertTrue(customStack.push("Test String.")),
                ()-> Assertions.assertEquals(1,customStack.length())
        );
    }

    @Test
    public void testCustomStackLength_whenZero(){
        CustomStack customStack = new CustomStack();
        Assertions.assertEquals(0,customStack.length());
    }

    @Test
    public void testCustomStackPeek(){
        CustomStack customStack = new CustomStack();
        customStack.push("Test String Peek.");
        Assertions.assertEquals("Test String Peek.", customStack.peek());
    }

    @Test
    public void testCustomStackPeek_whenNull(){
        CustomStack customStack = new CustomStack();
        Assertions.assertEquals(null, customStack.peek());
    }

    @Test
    public void testCustomStackPop_whenEmpty(){
        CustomStack customStack = new CustomStack();
        Assertions.assertThrows(NullPointerException.class, customStack::pop);
    }

    @Test
    public void testCustomStackPop(){
        CustomStack customStack = new CustomStack();
        customStack.push("Test String Pop");
        Node popped = customStack.pop();
        Assertions.assertAll(
                ()-> Assertions.assertEquals("Test String Pop",popped.getContents()),
                ()-> Assertions.assertEquals(customStack.getHead(),popped.getPrevious())
        );
    }

    @Test
    public void testCustomStackSearch_ifPresent(){
        CustomStack customStack = new CustomStack();
        int n = 34;
        for(int i = 0; i < 100; i++){
            customStack.push(i);
        }
        Assertions.assertEquals(customStack.length()-n,customStack.search(n));
    }
}
