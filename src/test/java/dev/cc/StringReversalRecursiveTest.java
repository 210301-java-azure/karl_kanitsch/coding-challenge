package dev.cc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StringReversalRecursiveTest {

    @Test
    public void testRecursiveStringReversal(){
        Assertions.assertEquals(".gnirtS tseT",StringReversalRecursive.recursiveStringReversal("Test String."));
    }
}
