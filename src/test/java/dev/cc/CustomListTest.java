package dev.cc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;

public class CustomListTest {

    @Test
    public void isEmptyTest_whenIsEmpty_thenReturnsTrue(){
        List<Object> list = new CustomList<>();
        Assertions.assertTrue(list.isEmpty());
    }

    @Test
    public void isEmptyTest_whenNotEmpty_thenReturnsFalse(){
        List<Object> list = new CustomList<>();
        list.add(null);
        Assertions.assertFalse(list.isEmpty());
    }

    @Test
    public void sizeTest_whenSingleItemExists_thenReturnsOne(){
        List<Object> list = new CustomList<>();
        list.add(null);
        Assertions.assertEquals(1,list.size());
    }

    @Test
    public void sizeTest_whenEmpty_thenReturnsZero(){
        List<Object> list = new CustomList<>();
        Assertions.assertEquals(0,list.size());
    }

    @Test
    public void sizeTest_whenTwoItems_thenReturnsTwo(){
        List<Object> list = new CustomList<>();
        list.add(null);
        list.add(null);
        Assertions.assertEquals(2,list.size());
    }

    @Test
    public void getTest_whenGet_thenReturnElement(){
        List<Object> list = new CustomList<>();
        list.add("Test String");
        Assertions.assertEquals("Test String",list.get(0));
    }

    @Test
    public void getTest_whenGetMultipleIndeces_thenReturnMultipleElements(){
        List<Object> list = new CustomList<>();
        list.add("Test String 1");
        list.add("Test String 2");
        Assertions.assertAll(
                ()-> Assertions.assertEquals("Test String 1", list.get(0)),
                ()-> Assertions.assertEquals("Test String 2", list.get(1))
        );
    }

    @Test
    public void addTest_whenAdded_thenReturnTrue(){
        List<Object> list = new CustomList<>();
        Assertions.assertTrue(list.add(null));
    }
}
