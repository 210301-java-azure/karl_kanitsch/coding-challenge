package dev.cc;

public class Fibonacci {

    public static void main(String[] args) {
        System.out.println(findNthFibonacci(12));
    }


    public static String findNthFibonacci(int n){
        if(n < 0)
            return "Invalid Input";
        StringBuilder output = new StringBuilder("The " + n);
        if(n > 3 && n < 21)
            output.append("th");
        else {
            switch (n % 10) {
                case 1:
                    output.append("st");
                    break;
                case 2:
                    output.append("nd");
                    break;
                case 3:
                    output.append("rd");
                    break;
                default:
                    output.append("th");
            }
        }
        output.append(" number in the Fibonacci Sequence is ");
        if(n == 0)
            output.append(String.valueOf(0));
        if(n == 1)
            output.append(String.valueOf(1));
        if(n > 1)
            output.append(String.valueOf(fibonacciValues(n)));
        return output.toString();
    }

    private static Integer fibonacciValues(int n){
        int i = 1;
        int x = 0;
        int y = 1;
        int z;
        do {
            z = x + y;
            x = y;
            y = z;
            i++;
        } while (i < n);
        return z;
    }
}
